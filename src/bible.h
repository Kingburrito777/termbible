//
// Created by liam on 8/25/20.
//

#ifndef TERMBIBLE_BIBLE_H
#define TERMBIBLE_BIBLE_H

#include <stdbool.h>

// 64 Bit OS -> 4 + 8 + 8 Bytes (24 Bytes - 3 CPU words - 0 padding)
struct verse {
    int verse_id;			        // verse number in chapter
    char* verse_text;		        // unknown size
    struct verse *next_verse; 	    // Next verse in chapter -- NULL if end of chapter
};

// 64 Bit OS -> 4 + 8 + 8 Bytes (24 Bytes - 3 CPU words - 0 padding)
struct chapter {
    int chapter_id;			        // Chapter number in book
    struct verse** chapter_verses;	// "list" of chapter verses
    struct chapter* next_chapter;	// link to next chapter in book -- NULL if end of book
};

// 64 Bit OS -> 4 + 4 + 8 + 8 + 8 Bytes (32 Bytes - 4 CPU words - 0 padding)
struct book {
    int book_id;			        // Numerical book id (1->66)
    char abbrev[5];		            // abbreviation of chapter - at most 4 bytes plus \0
    char* book_title;		        // Name of bible book
    struct chapter** book_chapters;	// "list" of chapters in the book
    struct book *next_book;		    // link to next book in the Bible -- NULL if end of bible
};

// 64 Bit OS -> 8 + 16 Bytes (24 bytes - 3 CPU words - 0 padding)
typedef struct {
    struct book **books;		    // "list" of books in the bible
    char bible_version_name[16];	// KJV, etc
} Biblebook;

#endif //TERMBIBLE_BIBLE_H
