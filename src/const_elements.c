//
// Created by liam on 8/31/20.
//
#include <stdlib.h>
#include "ConstUIElements.h"

char* link_window_titles[] = {
    "Annotations",
    "Gods Answers to Our Concerns",
    "Index to Annotations",
    "Miracles of Christ",
    "Parables",
    "Teachings"
};


struct ui_content_page annotations_page = {
        "Annotations",
        {
                {"Abiding in Christ",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Abundance",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Afflictions",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};

struct ui_content_page answers_page = {
        "Gods Answers to Our Concerns",
        {
                {"Test 1",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Test 2",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Test 3",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};

struct ui_content_page miracles_page = {
        "Miracles of Christ",
        {
                {"Test 1",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Test 2",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Test 3",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};

struct ui_content_page parables_page = {
        "Parables",
        {
                {"Abiding in Christ",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Abundance",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Afflictions",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};

struct ui_content_page teachings_page = {
        "Teachings",
        {
                {"Abiding in Christ",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Abundance",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Afflictions",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};


struct ui_content_page index_annotations_page = {
        "Index to Annotations",
        {
                {"Abiding in Christ",
                 {{32, 15, 5},
                  {62, 2, 28},
                  {63, 1, 9}}
                },
                {"Abundance",
                 {{5, 30, 9},
                  {32, 10, 10},
                  {47, 9, 8}}
                },
                {"Afflictions",
                 {{18, 5, 17},
                  {47, 4, 17},
                  {58, 12, 11}}
                }
        }
};