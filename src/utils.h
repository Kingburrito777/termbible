//
// Created by liam on 8/19/20.
//

#ifndef TERMBIBLE_UTILS_H
#define TERMBIBLE_UTILS_H

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bible.h"

// Must be given type**
#define ARR_SIZE(ptrs)   (sizeof(ptrs) / sizeof(ptrs[0]))

typedef enum growthResult {
    GROWTH_RESULT_SUCCESS = 1,
    GROWTH_RESULT_FAILURE_NOT_NECESSARY,
    GROWTH_RESULT_FAILURE_ALLOCATION
} growthResult;

typedef enum loadBibleResult {
    LOAD_BIBLE_SUCCESS = 1,
    LOAD_BIBLE_FAILURE_FILE_NOT_FOUND,
    LOAD_BIBLE_FAILURE_COULD_NOT_ALLOCATE,
    LOAD_BIBLE_FAILURE_BIBLE_ALREADY_LOADED
} loadBibleResult;

growthResult growthOptional(void **_grow, size_t currentLength, size_t newLength);
loadBibleResult loadBibleJSON(char* file_location, char** destination);
Biblebook *bibleBuilder(char* raw_json_text);

void bibleFree(Biblebook* bible);
void biblePrint(Biblebook* bible);

#endif //TERMBIBLE_UTILS_H