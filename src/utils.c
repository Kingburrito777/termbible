//
// Created by liam on 8/23/20.
//

#include "utils.h"
#include "../cjson/cJSON.h"

// Will need:
// 	Config loader
// 	User Annotations (writer) -> to file

/**
 * General pointer growth used to grow a pointer holding pointers.
 * @param _grow pointer holding pointers to grow
 * @param currentLength current length of pointers
 * @param newLength new desired length of pointers
 * @return result of that attempted memory growth
 */
growthResult growthOptional(void **_grow, size_t currentLength, size_t newLength)
{
    void *grow = *_grow;
    if (newLength > currentLength)
    {
        void *newGrowth = realloc(grow, newLength);
        if (newGrowth)
        {
            // Successfull resize
            *_grow = newGrowth;
            return GROWTH_RESULT_SUCCESS;
        }
        // Failure in reallocation
        return GROWTH_RESULT_FAILURE_ALLOCATION;
    }
    // Not necessary - Length not bigger
    return GROWTH_RESULT_FAILURE_NOT_NECESSARY;
}

/**
 * Load a JSON file into Memory. This function only reads ONE LINE using stdio's getline().
 * This should only be called with a JSON file that data consists on a single line.
 * @param file_location char* file location.
 * @param destination where the data will end up.
 * @return loading result.
 */
loadBibleResult loadBibleJSON(char* file_location, char** destination) {
    // Case file at location not found
    FILE *file = fopen(file_location, "r");
    if (!file) {
        fprintf(stderr, "Could not load file %s for reading", file_location);	//TODO make log
        return LOAD_BIBLE_FAILURE_FILE_NOT_FOUND;
    }

    // Create return pointer to direct *destination
    char* json_str_ptr = NULL;

    ssize_t read;
    size_t len;
    // Our bible JSON is only on one line -
    read = getline(&json_str_ptr, &len, file);

    // Somehow, if getline read fails
    if (json_str_ptr == NULL) {
        fprintf(stderr, "Could not allocate for file");
        return LOAD_BIBLE_FAILURE_COULD_NOT_ALLOCATE;
    }
    fclose(file);
    *destination = json_str_ptr;
    return LOAD_BIBLE_SUCCESS;
}

/**
 * Printf function for traversing Biblebook object and printing all elements.
 * That is every verse, in every chapter, in every book.
 * Would not be used in Ncurses context.
 * @param bible Biblebook struct object pointer
 */
void biblePrint(Biblebook* bible) {
    struct book* current = bible->books[0];
    while(current != NULL){
        printf("%s\n", current->book_title);
        struct chapter* current_c = current->book_chapters[0];
        while (current_c != NULL) {
            struct verse* current_v = current_c->chapter_verses[0];
            while (current_v != NULL) {
                printf("%d:%d %s ", current_c->chapter_id, current_v->verse_id, current_v->verse_text);
                current_v = current_v->next_verse;
            }
            printf("\n");
            current_c = current_c->next_chapter;
        }
        printf("\n\n");
        current = current->next_book;
    }
}

/**
 * Free the Biblebook struct object sequentially -
 * @param bible Biblebook struct object pointer
 */
void bibleFree(Biblebook* bible) {
    struct book* current = bible->books[0];
    while(current != NULL) {
        free(current->book_title);
        struct chapter* current_c = current->book_chapters[0];
        while (current_c != NULL) {
            struct verse* current_v = current_c->chapter_verses[0];
            while (current_v != NULL) {
                free(current_v->verse_text);
                struct verse* prev = current_v;
                current_v = current_v->next_verse;
                free(prev);
            }
            free(current_c->chapter_verses);
            struct chapter* prev = current_c;
            current_c = current_c->next_chapter;
            free(prev);
        }
        free(current->book_chapters);
        struct book* prev = current;
        current = current->next_book;
        free(prev);
    }
    free(bible->books);
    free(bible);
}

/**
 * Create Biblebook struct object from raw JSON -
 * @param raw_json_text raw json - whole text - single char*
 * @return Biblebook struct object pointer. Initialized and filled with JSON contents
 */
Biblebook *bibleBuilder(char* raw_json_text) {
    Biblebook *biblebook = (Biblebook*)calloc(1, sizeof(Biblebook));
    strncpy(biblebook->bible_version_name, "King James Bible", 16);

    // Extract and parse JSON raw char* using cJSON library
    cJSON *json_object = cJSON_Parse(raw_json_text);
    cJSON *book_json = NULL;
    // Allocate for incoming books
    int num_bible_books = cJSON_GetArraySize(json_object);
    biblebook->books = (struct book**)calloc(num_bible_books, sizeof(struct book*));

    // Goes through each book in the Bible JSON (1->66)
    int book_id = 1;
    struct book* previous_book = NULL;
    cJSON_ArrayForEach(book_json, json_object) {
        // Create new book struct pointer - Get book abbreviation and title from JSON object
        struct book* new_book = (struct book*)calloc(1, sizeof(struct book));
        char *book_abrev = cJSON_GetArrayItem(book_json, 0)->valuestring;
        char *book_title = cJSON_GetArrayItem(book_json, 2)->valuestring;
        // Copy abbreviation, title, and ID, to corresponding elements in struct
        strncpy(new_book->abbrev, book_abrev, 4);
        new_book->book_title = (char*)calloc(strlen(book_title) + 1, sizeof(char));
        strcpy(new_book->book_title, book_title);
        new_book->book_id = book_id;
        new_book->next_book = NULL; // Initialize "Next"

        // Add new book to current bible->books at correct position - array index
        biblebook->books[book_id - 1] = new_book;
        book_id++;

        // Add the "Next" book element and set previous to current
        if(previous_book != NULL)
            previous_book->next_book = new_book;
        previous_book = new_book;

        // Allocate pointers for book->chapters using length from json object
        cJSON *book_chapters_json = cJSON_GetArrayItem(book_json, 1);
        int num_book_chapters = cJSON_GetArraySize(book_chapters_json);
        new_book->book_chapters = (struct chapter**)calloc(num_book_chapters, sizeof(struct chapter*));

        // Start internal traversing: Iterates through all book chapters
        cJSON *chapter_json = NULL;
        int chapter_id = 1;
        struct chapter* previous_chapter = NULL;
        cJSON_ArrayForEach(chapter_json, book_chapters_json) {
            // Create new chapter struct pointer - Copy chapter ID, and initialize "Next"
            struct chapter* new_chapter = (struct chapter*)calloc(1, sizeof(struct chapter));
            new_chapter->chapter_id = chapter_id;
            new_chapter->next_chapter = NULL;

            // Add new chapter to current book->chapters
            new_book->book_chapters[chapter_id - 1] = new_chapter;
            chapter_id++;

            // Add "Next" chapter element and set previous to current
            if (previous_chapter != NULL)
                previous_chapter->next_chapter = new_chapter;
            previous_chapter = new_chapter;

            // Allocate pointers for chapter->verses using length from json object
            int num_verses = cJSON_GetArraySize(chapter_json);
            new_chapter->chapter_verses = (struct verse**)calloc(num_verses, sizeof(struct verse*));

            // Start internal traversing: Iterates through all chapter verses
            cJSON *verse_json = NULL;
            int verse_id = 1;
            struct verse* previous_verse = NULL;
            cJSON_ArrayForEach(verse_json, chapter_json) {
                // Create new verse struct - Copy verse ID, and initialize "Next"
                struct verse* new_verse = (struct verse*)calloc(1, sizeof(struct verse));
                new_verse->verse_id = verse_id;
                new_verse->next_verse = NULL;

                // Add new verse to current chapter->verses
                new_chapter->chapter_verses[verse_id - 1] = new_verse;
                verse_id++;

                // Add "Next" verse element and set previous to current
                if (previous_verse != NULL)
                    previous_verse->next_verse = new_verse;
                previous_verse = new_verse;

                // Copy text data from json object
                char *verse_text = cJSON_GetStringValue(verse_json);
                new_verse->verse_text = (char*)calloc(strlen(verse_text) + 1, sizeof(char));
                strcpy(new_verse->verse_text, verse_text);
            }   // End verses
        }   // End chapters
    }   // End books
    cJSON_Delete(json_object);
    free(raw_json_text);
    return biblebook;
}
