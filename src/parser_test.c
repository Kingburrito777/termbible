//
// Created by liam on 8/19/20.
//

// Parser tests for json Bible format
// TODO
#include "utils.h"
#include "../cjson/cJSON.h"
void test_sizes();

int main (int c, char** v)
{
    test_sizes();
    char* kjv_json_str = NULL;

    if (v[1])
    {
        loadBibleResult LOAD_RESULT = loadBibleJSON(v[1], &kjv_json_str);
        if (LOAD_RESULT == LOAD_BIBLE_SUCCESS)
        {
            printf("Successfully loaded\n\n");
            Biblebook *bible = bibleBuilder(kjv_json_str);
            printf("Title: %s\n", bible->bible_version_name);
            struct book* book_iterator = bible->books[0];
            while (book_iterator != NULL) {
                printf("Book ID: %d - Abbrev: %s - Name: %s\n",
                       book_iterator->book_id, book_iterator->abbrev, book_iterator->book_title);
                book_iterator = book_iterator->next_book;
            }

            //biblePrint(bible);
            bibleFree(bible);
        }
        printf("Return Value: %d\n", LOAD_RESULT);
    }
}

void test_sizes()
{
    printf("Verse struct size: %d\n", (int)sizeof(struct verse));
    printf("Chapter struct size %d\n", (int)sizeof(struct chapter));
    printf("Book struct size: %d\n", (int)sizeof(struct book));
    printf("Bible struct size: %d\n", (int)sizeof(Biblebook));
    printf("Sizeof int: %d\n", (int)sizeof(int));
    printf("Sizeof char: %d\n", (int)sizeof(char));
    printf("Sizeof bool: %d\n", (int)sizeof(bool));
}