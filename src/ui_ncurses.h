//
// Created by liam on 8/26/20.
//

#ifndef TERMBIBLE_UI_NCURSES_H
#define TERMBIBLE_UI_NCURSES_H

#include <ncurses.h>
#include <menu.h>
#include <form.h>
#include <panel.h>
#include "ConstUIElements.h"
#include "utils.h"

#define BIBLE_JSON "../data/en_kjv.json"
#define NUM_SLAVE_WINDOWS 6
#define ESC_KEY 	27

#define HEADER_HEIGHT 3
#define FOOTER_HEIGHT 1

static int max_y, max_x;

typedef struct ui_element_window {
    WINDOW *ui_window;
    int start_x, start_y;
    int size_x, size_y;
} ui_win;

typedef struct ui_element_text {
    //todo find out if needed
    //ui_win *parent_ui_window;
    int start_y, start_x;
    char* text;
    chtype text_attributes;
    chtype color_attributes;
} ui_element_text;

typedef struct ui_element_study_window {
    ui_content_page* content;
    ui_win *ui_element_win;
    PANEL *ui_element_study_window_panel;
    MENU *ui_element_study_window_menu;
    struct ui_element_study_window *next_ui_element_study_window;
} ui_element_study_window;

typedef struct ui_element_reader_window {
    ui_win *ui_element_win;
    ui_element_text **text_contents;
    //TODO add real elements
} ui_element_reader_window;

// ################ FUNCTIONS ######################
void greetingOnload();

// UI ELEMENT functions
void createUIElementsStudyWindows (ui_element_study_window **study_uis, ui_content_page* pages_contents[], int start_y, int start_x, int size_y, int size_x);
ui_element_study_window* createUIElementStudyWindow(PANEL *study_window_panel, ui_content_page* page_content, int start_y, int start_x, int size_y, int size_x, int label_color);

void createUIElementReaderWindow(ui_element_reader_window *reader_ui, char* initial_title, int start_y, int start_x, int size_y, int size_x);
void createUIElementGeneralWindow(ui_win *general_window, ui_element_text** text_elements, int start_y, int start_x, int size_y, int size_x);

void setUIWindowProperties(ui_win* ui_win);
void renderTextUIElement(ui_element_text *text_ui_element, ui_win *render_window);

// Removing these after better implementation
void printInMiddle(WINDOW *win, int starty, int startx, int width, char *string, chtype color);

// Onetime functions - start and end
int initCurses();
int cursesEntry(Biblebook *bible);
void ncursesDeinit();

#endif //TERMBIBLE_UI_NCURSES_H
