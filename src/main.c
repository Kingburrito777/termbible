// Termbible
// Liam L

#include "ui_ncurses.h"

int main()
{
    char *kjv_json_str = NULL;
    Biblebook *bible = NULL;
    loadBibleResult LOAD_RESULT = loadBibleJSON(BIBLE_JSON, &kjv_json_str);

    if (LOAD_RESULT != LOAD_BIBLE_SUCCESS) {
        fprintf(stderr, "Unable to load Bible json");
        exit(EXIT_FAILURE);
    } bible = bibleBuilder(kjv_json_str);

    if(initCurses() == -1) {
        exit(EXIT_FAILURE);
    }

    cursesEntry(bible);

    bibleFree(bible);
    ncursesDeinit();
}