//
// Created by liam on 8/29/20.
//

#ifndef TERMBIBLE_CONSTUIELEMENTS_H
#define TERMBIBLE_CONSTUIELEMENTS_H

struct bible_link {
    int lnk[3];
};

typedef struct ui_content_entry {
    char* content_title;
    struct bible_link content_links[5];
} ui_content_entry;

typedef struct ui_content_page {
    char* ui_page_title;
    ui_content_entry content_entries[];
} ui_content_page;

extern ui_content_page annotations_page;
extern ui_content_page answers_page;
extern ui_content_page miracles_page;
extern ui_content_page parables_page;
extern ui_content_page teachings_page;
extern ui_content_page index_annotations_page;

//"               |\n"
//"           \\       /\n"
//"             .---. \n"
//"        '-.  |   |  .-'\n"
//"          ___|   |___\n"
//"     -=  [           ]  =-\n"
//"         `---.   .---' \n"
//"      __||__ |   | __||__\n"
//"      '-..-' |   | '-..-'\n"
//"        ||   |   |   ||\n"
//"        ||_.-|   |-,_||\n"
//"      .-\"`   `\"`'`   `\"-.\n"
//"jgs .'                   '."

#endif //TERMBIBLE_CONSTUIELEMENTS_H
