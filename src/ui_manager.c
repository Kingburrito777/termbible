//
// Created by liam on 8/26/20.
//

// Funcs declared in ui_ncurses H
#include "ui_ncurses.h"

ui_element_study_window* ui_slave_windows[NUM_SLAVE_WINDOWS];
ui_content_page *slave_windows_content[NUM_SLAVE_WINDOWS];

ui_element_study_window *top_study_window;
ui_element_reader_window *ui_main_reader;

ui_win *ui_header;
ui_win *ui_footer;

ui_element_text *header_text_elements[1];

int userEntryLoop();
void cycleHelperWindows();

int cursesEntry(Biblebook *bible) {
    // Order of ops:
    // Init windows:
    //  Initialize header and footer
    //  Initialize study windows
    //  Initialize reader window (main focus window)
    slave_windows_content[0] = &annotations_page;
    slave_windows_content[1] = &teachings_page;
    slave_windows_content[2] = &answers_page;
    slave_windows_content[3] = &miracles_page;
    slave_windows_content[4] = &parables_page;
    slave_windows_content[5] = &index_annotations_page;

//    greetingOnload();
    int x_is = 0;
    int y_is = 0;
    header_text_elements[0] = (ui_element_text *)calloc(1, sizeof(ui_element_text));
    header_text_elements[0]->start_y = 1;
    header_text_elements[0]->start_x = 0;
    header_text_elements[0]->text = bible->bible_version_name;
    header_text_elements[0]->text_attributes = A_BOLD | A_ITALIC;
    header_text_elements[0]->color_attributes = 3;
    createUIElementGeneralWindow(ui_header, header_text_elements, y_is, x_is, HEADER_HEIGHT, COLS);

    y_is += 2;
    int s_height = LINES - 3;
    int s_width = (COLS/3) - 3;     // about a third of full size
    createUIElementsStudyWindows(ui_slave_windows, slave_windows_content,
                                 y_is, 1, s_height, s_width);

    x_is += (COLS/3) - 3;
    int r_height = LINES - 3;
    int r_width = COLS - x_is;      // about two thirds
    createUIElementReaderWindow(ui_main_reader, bible->bible_version_name,
                                y_is, x_is, r_height, r_width);
    // main loop
    userEntryLoop();
    return 0;
}

void settingsUpdate() {
    getmaxyx(stdscr, max_y, max_x);
}

int userEntryLoop() {
    int ch;
    while((ch = getch()) != 'q') {
        switch (ch) {
            case 9:
                cycleHelperWindows();
                break;
        }
        settingsUpdate();
        update_panels();
        doupdate();
    }
    return 0;
}

int initCurses() {
    // Initialize Cursor
    if (!initscr()) {
        fprintf(stderr, "Unable to initialize Ncurses");
        return -1;
    }
    // Enable Colors - standard exit - F1, F2, ..., Etc. Keys
    start_color();
    cbreak();
    keypad(stdscr, TRUE);

    /* Initialize all the colors */
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_CYAN, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_YELLOW, COLOR_BLACK);
    init_pair(7, COLOR_WHITE, COLOR_BLACK);

    // Dont write user input
    noecho();

    return 0;
}

void cycleHelperWindows( ) {
    top_study_window = top_study_window->next_ui_element_study_window;
    top_panel( (PANEL*) panel_userptr(top_study_window->ui_element_study_window_panel) );
}

// This will init all our bible study windows and link them together
// All of the panels/windows are the same size because they are cyclic
void createUIElementsStudyWindows (ui_element_study_window **study_uis, ui_content_page *pages_contents[],
                                   int start_y, int start_x, int size_y, int size_x) {
    ui_element_study_window *previous = NULL;
    for (int i = 0; i < NUM_SLAVE_WINDOWS; ++i) {
        PANEL *ui_study_window_panel = NULL;
        study_uis[i] = createUIElementStudyWindow(ui_study_window_panel, pages_contents[i],
                                                          start_y, start_x, size_y, size_x, 1);

        // Link to next Study UI
        if (previous != NULL)
            previous->next_ui_element_study_window = study_uis[i];
        previous = study_uis[i];

        if (i + 1 == NUM_SLAVE_WINDOWS)
            previous->next_ui_element_study_window = study_uis[0];
    }

    for (int i = 0; i < NUM_SLAVE_WINDOWS; i++) {
        PANEL* panel_src = study_uis[i]->ui_element_study_window_panel;
        if ((i + 1) == NUM_SLAVE_WINDOWS) // Last element links back to 0
            set_panel_userptr(panel_src, study_uis[0]->ui_element_study_window_panel);
        else
            set_panel_userptr(panel_src, study_uis[i+1]->ui_element_study_window_panel);
    }
    update_panels();
    top_study_window = study_uis[0];
}

// Creates a single Study window
ui_element_study_window* createUIElementStudyWindow(PANEL *study_window_panel, ui_content_page* page_content,
                                                    int start_y, int start_x, int size_y, int size_x, int label_color) {
    //todo free at some point
    //todo test if link window title holds without calloc
    ui_element_study_window *ret_win = (ui_element_study_window *)calloc(1, sizeof(ui_element_study_window));

    //ret_win->title = (char*)calloc(strlen(title) + 1, sizeof(char));
    //sprintf(ret_win->title, "%s", title);

    WINDOW *ui_study_window = newwin(size_y, size_x, start_y, start_x);
    ret_win->ui_element_win = (ui_win *)calloc(1, sizeof(ui_win));
    ret_win->ui_element_win->ui_window = ui_study_window;
    setUIWindowProperties(ret_win->ui_element_win);

    study_window_panel = new_panel(ui_study_window);
    ret_win->ui_element_study_window_panel = study_window_panel;

    // Window Attributes
    box(ui_study_window, 0, 0);
    mvwaddch(ui_study_window, 2, 0, ACS_LTEE);
    mvwhline(ui_study_window, 2, 1, ACS_HLINE, size_x - 2);
    mvwaddch(ui_study_window, 2, size_x - 1, ACS_RTEE);
    printInMiddle(ui_study_window, 1, 0, size_x, page_content->ui_page_title, 1);

    wrefresh(ui_study_window);

    return ret_win;
}

void createUIElementReaderWindow(ui_element_reader_window *reader_ui, char* initial_title,
                                 int start_y, int start_x, int size_y, int size_x) {
    // todo add place to free -
    // First text element (text_contents[0]) will be title
    reader_ui = (ui_element_reader_window *)calloc(1, sizeof(ui_element_reader_window));
    reader_ui->text_contents = (ui_element_text **)calloc(1, sizeof(ui_element_text*));
    reader_ui->text_contents[0] = (ui_element_text *)calloc(1, sizeof(ui_element_text));
    reader_ui->text_contents[0]->text = initial_title;  // MUST be allocated in memory somewhere

    // ELEMENT: UI WINDOW
    WINDOW *ui_reader_window = newwin(size_y, size_x, start_y, start_x);
    reader_ui->ui_element_win = (ui_win *)calloc(1, sizeof(ui_win));
    reader_ui->ui_element_win->ui_window = ui_reader_window;
    setUIWindowProperties(reader_ui->ui_element_win);

    // Window Attributes
    box(ui_reader_window, 0, 0);
    mvwaddch(ui_reader_window, 2, 0, ACS_LTEE);
    mvwhline(ui_reader_window, 2, 1, ACS_HLINE, size_x - 2);
    mvwaddch(ui_reader_window, 2, size_x - 1, ACS_RTEE);
    printInMiddle(ui_reader_window, 1, 0, size_x, initial_title, 1);
    wrefresh(ui_reader_window);
}

void createUIElementGeneralWindow(ui_win *general_window, ui_element_text** text_elements,
                                  int start_y, int start_x, int size_y, int size_x) {
    general_window = (ui_win *)calloc(1, sizeof(ui_win));
    WINDOW *general_window_ui = newwin(size_y, size_x, start_y, start_x);
    general_window->ui_window = general_window_ui;
    setUIWindowProperties(general_window);

    int ln = ARR_SIZE(text_elements);
    for (int i = 0; i < ln; ++i) {
        renderTextUIElement(text_elements[i], general_window);
    }
    refresh();
    wrefresh(general_window_ui);
}

// Sets the struct properties for (x, y), (x, y)
void setUIWindowProperties(ui_win* ui_win) {
    WINDOW *ui = ui_win->ui_window;
    int start_x, start_y, size_x, size_y;
    getbegyx(ui, start_y, start_x);
    getmaxyx(ui, size_y, size_x);
    ui_win->start_x = start_x;
    ui_win->start_y = start_y;
    ui_win->size_x = size_x;
    ui_win->size_y = size_y;
}

void renderTextUIElement(ui_element_text *text_ui_element, ui_win *render_window) {
    wattron(render_window->ui_window, COLOR_PAIR(text_ui_element->color_attributes));
    wattron(render_window->ui_window, text_ui_element->text_attributes);
    mvwprintw(render_window->ui_window, text_ui_element->start_y, text_ui_element->start_y, "%s", text_ui_element->text);
    wattroff(render_window->ui_window, text_ui_element->text_attributes);
    wattroff(render_window->ui_window, COLOR_PAIR(text_ui_element->color_attributes));
}

void printInMiddle(WINDOW *win, int starty, int startx, int width, char *string, chtype color) {
    int length, x, y;
    float temp;

    if(win == NULL)
        win = stdscr;
    getyx(win, y, x);
    if(startx != 0)
        x = startx;
    if(starty != 0)
        y = starty;
    if(width == 0)
        width = 80;

    length = strlen(string);
    temp = (width - length)/ 2;
    x = startx + (int)temp;
    wattron(win, color);
    mvwprintw(win, y, x, "%s", string);
    wattroff(win, color);
    refresh();
}

void greetingOnload()
{
    WINDOW *greeting_window;
    // Create new greeting window
    int height, width;
    getmaxyx(stdscr, height, width);
    greeting_window = newwin(height, width, 2, 2);

    box(greeting_window, 0, 0);
    wrefresh(greeting_window);

    // Get any character and exit current greeting window
    getch();

    werase(greeting_window);
    wrefresh(greeting_window);
    delwin(greeting_window);
}

void ncursesDeinit() {
    // Clear screen
    refresh();
    // End curses
    endwin();
}
